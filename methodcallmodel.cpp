/***************************************************************************
 *   Copyright 2015 Sandro Andrade <sandroandrade@kde.org>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/

#include "methodcallmodel.h"

#include <language/duchain/use.h>
#include <language/duchain/identifier.h>
#include <language/duchain/duchainlock.h>
#include <language/duchain/declaration.h>
#include <language/duchain/functiondefinition.h>
#include <language/duchain/classfunctiondeclaration.h>
#include <language/duchain/types/typeutils.h>
#include <language/duchain/types/pointertype.h>
#include <language/duchain/types/functiontype.h>
#include <language/duchain/types/structuretype.h>
#include <language/duchain/types/abstracttype.h>
#include <language/duchain/types/identifiedtype.h>
#include <language/duchain/classmemberdeclaration.h>

#include <language/classmodel/classmodel.h>
#include <language/classmodel/classmodelnode.h>

#include <QtGui/QStandardItemModel>

MethodCallModel::MethodCallModel(ClassModel *classModel)
  : m_classModel(classModel)
{
    KDevelop::DUChainReadLocker readLock(KDevelop::DUChain::lock());

    m_treeLeafIndexes.clear();
    populateTreeLeafIndexes(classModel->index(0, 0, QModelIndex()));
    int i = 0, size = m_treeLeafIndexes.size();
    if (size == 1)
        return;
    QModelIndex index, otherIndex;
    while (i < size)
    {
        QModelIndex index = m_treeLeafIndexes.values().at(i);
        KDevelop::DeclarationPointer declaration = DeclarationPointer( dynamic_cast<KDevelop::Declaration*>( m_classModel->duObjectForIndex( index ) ) );
        Declaration* decl = declaration.data();
        FunctionDefinition* funcDefinition = 0;
        // If it's a function, find the function definition to go to the actual declaration.
        if ( decl && decl->isFunctionDeclaration() )
        {
            funcDefinition = dynamic_cast<FunctionDefinition*>(decl);
            if ( funcDefinition == 0 )
                funcDefinition = FunctionDefinition::definition(decl);
        }
        if (funcDefinition)
        {
            DUContext *context = funcDefinition->internalContext()->childContexts().at(0);
            const Use *uses = context->uses();
            unsigned int usesCount = context->usesCount();
            Declaration *declaration;
            for (unsigned int i = 0; i < usesCount; ++i)
            {
                declaration = context->topContext()->usedDeclarationForIndex(uses[i].m_declarationIndex);
                if (declaration && declaration->isFunctionDeclaration())
                {
                    QModelIndex calleeIndex = m_treeLeafIndexes[declaration->qualifiedIdentifier().toString()];
                    if (calleeIndex.isValid()) {
                        qDebug() << "Found function use of" << declaration->qualifiedIdentifier().toString() << ", index:" << calleeIndex;
                        QStandardItem *source = new QStandardItem;
                        source->setData(QVariant::fromValue(index), Qt::UserRole);
                        QStandardItem *target = new QStandardItem;
                        target->setData(QVariant::fromValue(calleeIndex), Qt::UserRole);
                        invisibleRootItem()->appendRow(QList<QStandardItem *>() << source << target);
                    }
                }
            }
        }
        ++i;
    }
}

MethodCallModel::~MethodCallModel()
{
}

void MethodCallModel::populateTreeLeafIndexes(const QModelIndex &index)
{
    int rowCount = m_classModel->rowCount(index);
    if (rowCount == 0) {
        KDevelop::Declaration *decl = dynamic_cast<KDevelop::Declaration*>(m_classModel->duObjectForIndex(index));
        if (decl) {
            m_treeLeafIndexes[decl->qualifiedIdentifier().toString()] = index;
        }
    }
    else
        for (int row = 0; row < rowCount; ++row)
            populateTreeLeafIndexes(m_classModel->index(row, 0, index));
}

QModelIndex MethodCallModel::getIndexForIdentifier(const KDevelop::IndexedQualifiedIdentifier& a_id)
{
    return QModelIndex();
}

// kate: space-indent on; indent-width 2; tab-width 4; replace-tabs on; auto-insert-doxygen on
