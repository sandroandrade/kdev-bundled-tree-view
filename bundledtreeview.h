/***************************************************************************
 *   Copyright 2012 Sandro Andrade <sandroandrade@kde.org>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/

#ifndef BUNDLEDTREEVIEW_H
#define BUNDLEDTREEVIEW_H

#include "ui_bundledtreeview.h"

class QStandardItem;
class KDevBundledTreeViewPlugin;
class ClassModel;

class BundledTreeView : public QWidget, public Ui::BundledTreeView
{
    Q_OBJECT

public:
    explicit BundledTreeView(KDevBundledTreeViewPlugin *plugin, QWidget *parent = 0);
    virtual ~BundledTreeView();
    
private Q_SLOTS:
    void refresh();
    
    void on_spbNodeRadius_valueChanged(double value);
    void on_sldInterSpanSpacing_valueChanged(int value);
    void on_sldInterLevelSpacing_valueChanged(int value);
    void on_chbShowNodes_toggled(bool value);
    void on_chbShowEdges_toggled(bool value);
    void on_chbShowLevelCircles_toggled(bool value);

    void on_sldMirroredRadiusIncrementFactor_valueChanged(int value);
    void on_sldBundlingStrengthFactor_valueChanged(int value);
    void on_sldAdjacencyEdgePenWidth_valueChanged(int value);
    void on_chbShowMirroredRadial_toggled(bool value);
    void on_chbShowAdjacencyEdges_toggled(bool value);

private:
    void expand(const QModelIndex &index);
  
    KDevBundledTreeViewPlugin *m_plugin;
    ClassModel *m_model;
    bool m_lastShowAdjacencyModel;
};

#endif
