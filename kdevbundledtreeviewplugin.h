/***************************************************************************
 *   Copyright 2012 Sandro Andrade <sandroandrade@kde.org>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/

#ifndef KDEVBUNDLEDTREEVIEWPLUGIN_H
#define KDEVBUNDLEDTREEVIEWPLUGIN_H

#include <QtCore/QVariant>

#include <interfaces/iplugin.h>

class BundledTreeView;
class KDevBundledTreeViewFactory;

class KDevBundledTreeViewPlugin : public KDevelop::IPlugin
{
    Q_OBJECT

public:
    explicit KDevBundledTreeViewPlugin(QObject *, const QVariantList & = QVariantList());
    virtual ~KDevBundledTreeViewPlugin();

    virtual void unload();

    void registerToolView(BundledTreeView *view);
    void unRegisterToolView(BundledTreeView *view);

public Q_SLOTS:
    void setActiveToolView(BundledTreeView *activeToolView);

private:
    BundledTreeView *activeToolView();
    KDevBundledTreeViewFactory *m_toolViewFactory;
    QList<BundledTreeView *> m_toolViews;
    BundledTreeView *m_activeToolView;
};

#endif
