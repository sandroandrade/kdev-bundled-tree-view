/***************************************************************************
 *   Copyright 2012 Sandro Andrade <sandroandrade@kde.org>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/

#include "bundledtreeview.h"

#include <QtCore/QTimer>
#include <QtGui/QStandardItemModel>

#include <interfaces/icore.h>
#include <interfaces/ilanguagecontroller.h>
#include <interfaces/iprojectcontroller.h>
#include <language/backgroundparser/backgroundparser.h>

#include "kdevbundledtreeviewplugin.h"
#include <language/classmodel/classmodel.h>

#include "methodcallmodel.h"

#define PI 3.14159265358979

Q_DECLARE_METATYPE(QModelIndex)

BundledTreeView::BundledTreeView(KDevBundledTreeViewPlugin *plugin, QWidget *parent)
:
QWidget(parent),
m_plugin(plugin),
m_model(new ClassModel)
{
    setupUi(this);

    connect(psbResetZoom, SIGNAL(clicked()), radialGraphicsTreeView, SLOT(resetZoom()));
    connect(psbResetPan, SIGNAL(clicked()), radialGraphicsTreeView, SLOT(resetPan()));

    connect(KDevelop::ICore::self()->languageController()->backgroundParser(), SIGNAL(parseJobFinished(KDevelop::ParseJob*)),
	    this, SLOT(refresh()));
    
    connect(KDevelop::ICore::self()->projectController(), SIGNAL(projectClosed(KDevelop::IProject*)),
	    this, SLOT(refresh()));
    connect(KDevelop::ICore::self()->projectController(), SIGNAL(projectOpened(KDevelop::IProject*)),
	    this, SLOT(refresh()));

    m_model->setFeatures(NodesModelInterface::ClassInternals);
    radialGraphicsTreeView->setTreeModel(m_model);
    
    refresh();
}

BundledTreeView::~BundledTreeView()
{
    m_plugin->unRegisterToolView(this);
}

void BundledTreeView::refresh()
{
    expand(m_model->index(0, 0, QModelIndex()));
    radialGraphicsTreeView->resetZoom();
    radialGraphicsTreeView->resetPan();
    radialGraphicsTreeView->setAdjacencyModel(new MethodCallModel(m_model));
    radialGraphicsTreeView->updateTreeScene();
}

void BundledTreeView::on_spbNodeRadius_valueChanged(double value)
{
    radialGraphicsTreeView->setNodeRadius(value);
    radialGraphicsTreeView->updateTreeScene();
}

void BundledTreeView::on_sldBundlingStrengthFactor_valueChanged(int value)
{
    radialGraphicsTreeView->setBundlingStrengthFactor(value/100.0);
    radialGraphicsTreeView->updateAdjacencyScene();
}

void BundledTreeView::on_sldAdjacencyEdgePenWidth_valueChanged(int value)
{
    radialGraphicsTreeView->setAdjacencyEdgePenWidth(value/100.0);
    radialGraphicsTreeView->updateAdjacencyScene();
}

void BundledTreeView::on_chbShowMirroredRadial_toggled(bool value)
{
    radialGraphicsTreeView->setViewFlag(RadialGraphicsTreeView::ShowMirroredRadialView, value);
    radialGraphicsTreeView->updateTreeScene();
    if (!value)
    {
        m_lastShowAdjacencyModel = chbShowAdjacencyEdges->isChecked();
        radialGraphicsTreeView->setViewFlag(RadialGraphicsTreeView::ShowAdjacencyModel, false);
        chbShowAdjacencyEdges->setChecked(false);
    }
    else
    {
        radialGraphicsTreeView->setViewFlag(RadialGraphicsTreeView::ShowAdjacencyModel, m_lastShowAdjacencyModel);
        chbShowAdjacencyEdges->setChecked(m_lastShowAdjacencyModel);
    }
}

void BundledTreeView::on_chbShowAdjacencyEdges_toggled(bool value)
{
    radialGraphicsTreeView->setViewFlag(RadialGraphicsTreeView::ShowAdjacencyModel, value);
    radialGraphicsTreeView->updateTreeScene();
}

void BundledTreeView::on_sldInterSpanSpacing_valueChanged(int value)
{
    radialGraphicsTreeView->setInterSpanSpacing((value*PI)/180.0);
    radialGraphicsTreeView->updateTreeScene();
}

void BundledTreeView::on_sldInterLevelSpacing_valueChanged(int value)
{
    radialGraphicsTreeView->setInterLevelSpacing(value);
    radialGraphicsTreeView->updateTreeScene();
}

void BundledTreeView::on_chbShowNodes_toggled(bool value)
{
    radialGraphicsTreeView->setViewFlag(RadialGraphicsTreeView::ShowNormalRadialViewNode, value);
    radialGraphicsTreeView->updateTreeScene();
}

void BundledTreeView::on_chbShowEdges_toggled(bool value)
{
    radialGraphicsTreeView->setViewFlag(RadialGraphicsTreeView::ShowNormalRadialViewEdge, value);
    radialGraphicsTreeView->updateTreeScene();
}

void BundledTreeView::on_chbShowLevelCircles_toggled(bool value)
{
    radialGraphicsTreeView->setViewFlag(RadialGraphicsTreeView::ShowNormalRadialViewLevelCircles, value);
    radialGraphicsTreeView->updateTreeScene();
}

void BundledTreeView::on_sldMirroredRadiusIncrementFactor_valueChanged(int value)
{
    radialGraphicsTreeView->setMirroredRadiusIncrementFactor(value/100.0);
    radialGraphicsTreeView->updateTreeScene();
}

void BundledTreeView::expand(const QModelIndex &index)
{
    m_model->expanded(index);
    int rowCount = m_model->rowCount(index);
    //kDebug() << "ROWCOUNT of" << index.data(Qt::DisplayRole).toString() << ":" << rowCount;
    for (int row = 0; row < rowCount; ++row)
	expand(m_model->index(row, 0, index));
}

