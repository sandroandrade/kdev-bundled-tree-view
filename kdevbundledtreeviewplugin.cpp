/***************************************************************************
*   Copyright 2012 Sandro Andrade <sandroandrade@kde.org>                 *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU Library General Public License as       *
*   published by the Free Software Foundation; either version 2 of the    *
*   License, or (at your option) any later version.                       *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU Library General Public     *
*   License along with this program; if not, write to the                 *
*   Free Software Foundation, Inc.,                                       *
*   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
***************************************************************************/

#include "kdevbundledtreeviewplugin.h"

#include <KPluginFactory>

#include <interfaces/icore.h>
#include <interfaces/iuicontroller.h>

#include "bundledtreeview.h"

K_PLUGIN_FACTORY_WITH_JSON(BundledTreeViewFactory, "kdevbundledtreeviewplugin.json", registerPlugin<KDevBundledTreeViewPlugin>(); )

class KDevBundledTreeViewFactory: public KDevelop::IToolViewFactory
{
public:
    KDevBundledTreeViewFactory(KDevBundledTreeViewPlugin *plugin) : m_plugin(plugin) {}
    virtual QWidget* create(QWidget *parent = 0)
    {
	return new BundledTreeView(m_plugin, parent);
    }
    virtual Qt::DockWidgetArea defaultPosition()
    {
	return Qt::RightDockWidgetArea;
    }
    virtual QString id() const
    {
	return "org.kdevelop.BundledTreeView";
    }
  
private:
  KDevBundledTreeViewPlugin *m_plugin;
};

KDevBundledTreeViewPlugin::KDevBundledTreeViewPlugin (QObject *parent, const QVariantList &) :
    KDevelop::IPlugin (QStringLiteral("kdevbundledtreeview"), parent),
    m_toolViewFactory(new KDevBundledTreeViewFactory(this)),
    m_activeToolView(0)
{
    core()->uiController()->addToolView(i18n("Bundled Tree View"), m_toolViewFactory);
}

KDevBundledTreeViewPlugin::~KDevBundledTreeViewPlugin()
{
}

void KDevBundledTreeViewPlugin::unload()
{
    core()->uiController()->removeToolView(m_toolViewFactory);
}

void KDevBundledTreeViewPlugin::registerToolView(BundledTreeView *view)
{
    m_toolViews << view;
}

void KDevBundledTreeViewPlugin::unRegisterToolView(BundledTreeView *view)
{
    m_toolViews.removeAll(view);
}

void KDevBundledTreeViewPlugin::setActiveToolView(BundledTreeView *activeToolView)
{
    m_activeToolView = activeToolView;
}

#include "kdevbundledtreeviewplugin.moc"
