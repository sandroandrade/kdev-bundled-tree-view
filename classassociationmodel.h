/***************************************************************************
 *   Copyright 2015 Sandro Andrade <sandroandrade@kde.org>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/

#ifndef CLASSASSOCIATIONMODEL_H
#define CLASSASSOCIATIONMODEL_H

#include <QtGui/QStandardItemModel>

class ClassModel;

class ClassAssociationModel : public QStandardItemModel
{
  Q_OBJECT

public:
  ClassAssociationModel(ClassModel *classModel);
  virtual ~ClassAssociationModel();

private:
  void populateTreeLeafIndexes(const QModelIndex &index);

private:
  ClassModel* m_classModel;
  QList<QModelIndex> m_treeLeafIndexes;
};

#endif

// kate: space-indent on; indent-width 2; tab-width 4; replace-tabs on; auto-insert-doxygen on

