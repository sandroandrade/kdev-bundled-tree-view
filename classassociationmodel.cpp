/***************************************************************************
 *   Copyright 2015 Sandro Andrade <sandroandrade@kde.org>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/

#include "classassociationmodel.h"

#include <language/duchain/identifier.h>
#include <language/duchain/duchainlock.h>
#include <language/duchain/declaration.h>
#include <language/duchain/classdeclaration.h>
#include <language/duchain/types/typeutils.h>
#include <language/duchain/types/pointertype.h>
#include <language/duchain/types/structuretype.h>
#include <language/duchain/types/abstracttype.h>
#include <language/duchain/types/identifiedtype.h>
#include <language/duchain/classmemberdeclaration.h>

#include <language/classmodel/classmodel.h>
#include <language/classmodel/classmodelnode.h>

#include <QtGui/QStandardItemModel>

ClassAssociationModel::ClassAssociationModel(ClassModel *classModel)
  : m_classModel(classModel)
{
    KDevelop::DUChainReadLocker readLock(KDevelop::DUChain::lock());

    m_treeLeafIndexes.clear();
    populateTreeLeafIndexes(classModel->index(0, 0, QModelIndex()));
    int i = 0, size = m_treeLeafIndexes.size();
    if (size == 1)
            return;
    QModelIndex index, otherIndex;
    while (i < size)
    {
        QModelIndex index = m_treeLeafIndexes.at(i);
        KDevelop::DUChainBase *duObject = m_classModel->duObjectForIndex(index);
        KDevelop::ClassDeclaration *klass;
        if ((klass = dynamic_cast<KDevelop::ClassDeclaration*>(duObject)))
        {
            qDebug() << "Classe: " << klass->identifier().toString();
            foreach(KDevelop::Declaration* decl, klass->internalContext()->localDeclarations())
            {
                if ( decl->isFunctionDeclaration() );
                else if ( KDevelop::ClassMemberDeclaration* memDecl = dynamic_cast<KDevelop::ClassMemberDeclaration*>(decl) ) {
                    qDebug() << "Attribute: " << memDecl->identifier().toString();
                    if (KDevelop::StructureType::Ptr klass = memDecl->type<KDevelop::StructureType>()) {
                        KDevelop::ClassDeclaration* classTypeDecl = dynamic_cast<KDevelop::ClassDeclaration*>(klass->declaration(decl->topContext()));
                        qDebug() << "type " << classTypeDecl->identifier().toString();
                    }
                    else if (KDevelop::PointerType::Ptr pklass = memDecl->type<KDevelop::PointerType>()) {
                        if (KDevelop::StructureType* aklass = dynamic_cast<KDevelop::StructureType*>(pklass->baseType().data())) {
                            qDebug() << "Achei ponteiro " << decl->identifier().toString();
                            KDevelop::AbstractType::Ptr target = TypeUtils::targetTypeKeepAliases( pklass, decl->topContext() );
                            const KDevelop::IdentifiedType* idType = dynamic_cast<const KDevelop::IdentifiedType*>( target.data() );
                            if ( KDevelop::ClassDeclaration* classTypeDecl = dynamic_cast<KDevelop::ClassDeclaration*>(idType->declaration(decl->topContext() )) ) {
                                qDebug() << "type " << classTypeDecl->identifier().toString();
                                QModelIndex associationIndex = m_classModel->getIndexForIdentifier(classTypeDecl->qualifiedIdentifier());
                                qDebug() << "Index:" << associationIndex;
                                QStandardItem *source = new QStandardItem;
                                source->setData(QVariant::fromValue(index), Qt::UserRole);
                                QStandardItem *target = new QStandardItem;
                                target->setData(QVariant::fromValue(associationIndex), Qt::UserRole);
                                invisibleRootItem()->appendRow(QList<QStandardItem *>() << source << target);
                             }
                        }
                    }
                }
            }
        }
/*
        if (i % 10 == 0)
            otherIndex = m_treeLeafIndexes.at(qrand() % size);
        while ((index = m_treeLeafIndexes.at(qrand() % size)) == otherIndex);
        QStandardItem *source = new QStandardItem;
        source->setData(QVariant::fromValue(index), Qt::UserRole);
        QStandardItem *target = new QStandardItem;
        target->setData(QVariant::fromValue(otherIndex), Qt::UserRole);
        invisibleRootItem()->appendRow(QList<QStandardItem *>() << source << target);
*/
        ++i;
    }
}

ClassAssociationModel::~ClassAssociationModel()
{
}

void ClassAssociationModel::populateTreeLeafIndexes(const QModelIndex &index)
{
    int rowCount = m_classModel->rowCount(index);
    if (rowCount == 0)
        m_treeLeafIndexes.append(index);
    else
        for (int row = 0; row < rowCount; ++row)
            populateTreeLeafIndexes(m_classModel->index(row, 0, index));
}

// kate: space-indent on; indent-width 2; tab-width 4; replace-tabs on; auto-insert-doxygen on
